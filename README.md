# Utility Scripts

Scripts for automation of day-to-day tasks on engineering machines. 

## List Deployed Versions

It is often necessary to check version of code currently deployed on remote environments (dev, test, nft etc.). 

This requires logging into AWS console and browsing to each running service definition and noting image tag version.  This can often take several minutes.

`list-deployed-versions.sh` script can instead be used to list currently deployed version of all microservices that enable OIDV project as a whole. 

## Prerequisites
### CLI Tools
- `aws cli` 
- `jq`

See [Install CLI Tools](https://gitlab.com/dwp/dynamic-trust-hub/forgerock/developer-guidelines/-/wikis/Install-the-required-tools#install-cli-tools) in developer guide.

### Access to ID&T non-prod AWS Account (shefcon-dev)
See [Setup & Configure AWS CLI](https://gitlab.com/dwp/dynamic-trust-hub/forgerock/developer-guidelines/-/wikis/Setup-&-Configure-AWS-CLI) for steps involved in getting and configuring AWS non-prod account.

## Installation
Recommended installation steps: 
```shell
cd ~
git clone git@gitlab.com:ravish.bhagdev1/util-scripts.git
echo "export PATH=\"\$HOME/util-scripts:\$PATH\"" >> ~/.zshrc
source ~/.zshrc
```
You will be able to execute the script from anywhere after installation.
### Usage
```shell
list-deployed-versions.sh [environment]
```
Currently supported values for environment:
- dev
- test
- nft
### Examples

#### NFT Environment
```shell
list-deployed-versions.sh nft
```
```shell
====================================================

			SHARED
====================================================

AM-nft                   	v2.2.0-RC1     

idt-nft-iga              	v1-0-4-6a21f27e
idt-nft-idpolicy         	v2-0-4-rc1-bb7d8db3
idt-nft-idstore          	v1-0-1-rc1-7e446552
idt-nft-activityhistory  	v2-0-3-rc1-4a8d8505
idt-nft-counterfraud     	v2-0-5-rc1-11f3dbf0
dth-nft-kbv              	develop-79e3c8e1


====================================================

			OIDV
====================================================

idt-nft-ui               	v2-4-0-rc1-ea803dd0
idt-nft-corebio-jr       	v2-0-2-rc1-c0f2c3db
idt-nft-ebv-hmrc         	v2-0-7-rc1-83bd149f


====================================================

			TIDV
====================================================

cxp-nft-getpaymentsesa   	develop-c4db38cc
idt-nft-am               	v1-0-4-11332a73
idt-nft-idvfailure       	v1-0-0-aae9b895
cxp-nft-payments         	develop-23905ad6
cxp-nft-bacs             	develop-39956920
```
Test Environment
```shell
list-deployed-versions.sh test
```
```shell
====================================================

			SHARED
====================================================

AM-test                  	v2.2.1-RC1     

idt-test-iga             	v1-0-4-6a21f27e
idt-test-idpolicy        	v2-0-4-rc1-bb7d8db3
idt-test-idstore         	v1-0-1-rc1-7e446552
idt-test-activityhistory 	v2-0-3-rc1-4a8d8505
idt-test-counterfraud    	v2-0-5-rc1-11f3dbf0
dth-test-kbv             	develop-2b2f2c95


====================================================

			OIDV
====================================================

idt-test-ui              	v2-4-1-rc1-6d7f5f00
idt-test-corebio-jr      	v2-0-2-rc1-c0f2c3db
idt-test-ebv-hmrc        	v2-0-7-rc1-83bd149f


====================================================

			TIDV
====================================================

cxp-test-getpaymentsesa  	develop-c4db38cc
idt-test-am              	develop-ad6f7bb8
idt-test-idvfailure      	develop-39bdeec4
cxp-test-payments        	develop-d43a4c93
cxp-test-bacs            	develop-80f1da4c
```

#### Dev Environment
```shell
list-deployed-versions.sh dev
```
```shell
====================================================

			SHARED
====================================================

AM-dev                   	develop        

idt-dev-iga              	develop-fc752d7b
idt-dev-idpolicy         	develop-a8912dd1
idt-dev-idstore          	develop-f8f33e67
idt-dev-activityhistory  	develop-001a278e
idt-dev-counterfraud     	develop-2659b880
dth-dev-kbv              	develop-2b2f2c95


====================================================

			OIDV
====================================================

idt-dev-ui               	develop-0fd71981
idt-dev-corebio-jr       	develop-e202e578
idt-dev-ebv-hmrc         	develop-76c65d6f


====================================================

			TIDV
====================================================

cxp-dev-getpaymentsesa   	develop-c4db38cc
idt-dev-am               	develop-96ec4851
idt-dev-idvfailure       	develop-39bdeec4
cxp-dev-payments         	develop-d43a4c93
cxp-dev-bacs             	develop-80f1da4c
```

## List GOV.UK One Login Service Versions

It is often necessary to check version of code currently deployed on remote environments (dev, test, staging etc.).

This requires using k9s and opening each pod individually.

`list-ol-versions.sh` script can instead be used to list currently deployed version of all microservices on HCS that enable OL project.

### Examples

#### Dev Environment
```shell
list-ol-versions.sh
```

```shell
Namespace                                Pod                                                Status          Restarts   Version             
---------                                ---------------------------------------            ---------       --------   -------             
idt-extidentity-account                 
                                         ext-account-service-main-5d946b487b-5z5j7          Running         0          v1-2-0-ae251614     
idt-extidentity-account                 
                                         ext-account-service-main-5d946b487b-svkj7          Running         0          v1-2-0-ae251614     
-----------------------------------------------------------------------------------------------------------------------------------------------
idt-extidentity-account-link            
                                         ext-account-link-svc-main-d779b65cd-nm26d          Running         0          v1-2-0-0440466f     
idt-extidentity-account-link            
                                         ext-account-link-svc-main-d779b65cd-tjntr          Running         0          v1-2-0-0440466f     
-----------------------------------------------------------------------------------------------------------------------------------------------
idt-extidentity-encryption              
                                         idt-encryption-svc-main-58c9fd5f5b-hncm7           Running         0          v1-2-4-759a1bd5-1474899384
idt-extidentity-encryption              
                                         idt-encryption-svc-main-58c9fd5f5b-scd58           Running         0          v1-2-4-759a1bd5-1474899384
-----------------------------------------------------------------------------------------------------------------------------------------------
idt-extidentity-evidence-store          
                                         idt-evidence-store-main-fc6cb8f54-8lnsm            Running         0          v1-2-0-53cfac87     
idt-extidentity-evidence-store          
                                         idt-evidence-store-main-fc6cb8f54-phvs7            Running         0          v1-2-0-53cfac87     
-----------------------------------------------------------------------------------------------------------------------------------------------
idt-extidentity-federation              
                                         federation-service-main-7cc55f8d75-ck7nb           Running         0          v1-2-0-accd9c62     
idt-extidentity-federation              
                                         federation-service-main-7cc55f8d75-wpgwb           Running         0          v1-2-0-accd9c62     
-----------------------------------------------------------------------------------------------------------------------------------------------
idt-extidentity-matching                
                                         idt-matching-bff-main-65db66cb85-n9c9v             Running         0          v1-1-2-f7c5b40a     
idt-extidentity-matching                
                                         idt-matching-bff-main-65db66cb85-pdm9k             Running         0          v1-1-2-f7c5b40a     
-----------------------------------------------------------------------------------------------------------------------------------------------
idt-extidentity-orchestration           
                                         orchestration-service-main-7fcf46f575-8zrvl        Running         0          v1-3-0-53bc0d9a     
idt-extidentity-orchestration           
                                         orchestration-service-main-7fcf46f575-c77v5        Running         0          v1-3-0-53bc0d9a     
-----------------------------------------------------------------------------------------------------------------------------------------------
idt-extidentity-stub                    
                                         express-monorepo-main-7765f5fff-5z2kg              Running         0          v1-0-17-b830f1d5    
idt-extidentity-stub                    
                                         express-monorepo-main-7765f5fff-cgjxc              Running         0          v1-0-17-b830f1d5    
-----------------------------------------------------------------------------------------------------------------------------------------------
```

## Authors and acknowledgment
Ravish Bhagdev
***
