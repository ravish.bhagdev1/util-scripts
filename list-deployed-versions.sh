function list() {
    arr=("$@")
    for td in "${arr[@]}";
    do
      image=$(aws ecs describe-task-definition --task-definition $td | jq '.taskDefinition.containerDefinitions[1].image')
      version=($(echo $image | tr -d '"' | tr ':' '\n'))
      printf "%-25s\t%15s\n" "$td" "${version[1]}"
    done
}

case "$1" in
  ("help") echo "usage: $0 [tidv/oidv] [dev/test/nft]"; exit ;;
  ("dev"|"test"|"nft")
    sharedTaskDefs=("idt-$1-iga" "idt-$1-idpolicy" "idt-$1-idstore" "idt-$1-activityhistory" "idt-$1-counterfraud" "dth-$1-kbv")
    tidvTaskDefs=("cxp-$1-getpaymentsesa" "idt-$1-am" "idt-$1-idvfailure" "cxp-$1-payments" "cxp-$1-bacs")
    oidvTaskDefs=("idt-$1-ui" "idt-$1-corebio-jr" "idt-$1-ebv-hmrc")
    input=$1
    envCaps=$(echo "$input" | tr 'a-z' 'A-Z');
    ec2Name="NONPROD/FR/$envCaps/AM/BLUE";;
  ?) echo "error: option -$1 is not implemented"; exit ;;
esac

amiId=$(aws ec2 describe-instances --filters "Name=tag:Name,Values=$ec2Name" 2> /dev/null | jq 2> /dev/null '.Reservations[0].Instances[0].ImageId' | tr -d '"')

if [ -z "$amiId" ]; then
  echo "AM not running"
fi

amVersion=$(aws ec2 describe-images --image-ids $amiId 2> /dev/null | jq -r '.Images | .[0].Tags | map(select(.Key=="ForgeOps Branch")|.Value) | .[0]')

printf "=%.0s"  $(seq 1 52)
echo "\n"
echo "\t\t\tSHARED"
printf "=%.0s"  $(seq 1 52)
echo "\n"
printf "%-25s\t%-15s" "AM-$1" "${amVersion}"
echo "\n"
list "${sharedTaskDefs[@]}"
echo "\n"
printf "=%.0s"  $(seq 1 52)
echo "\n"
echo "\t\t\tOIDV"
printf "=%.0s"  $(seq 1 52)
echo "\n"
list "${oidvTaskDefs[@]}"
echo "\n"
printf "=%.0s"  $(seq 1 52)
echo "\n"
echo "\t\t\tTIDV"
printf "=%.0s"  $(seq 1 52)
echo "\n"
list "${tidvTaskDefs[@]}"
