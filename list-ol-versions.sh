#!/bin/bash

# Define color codes
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
RED='\033[0;31m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color (reset)

# Define the prefix for the namespaces
NAMESPACE_PREFIX="idt-extidentity-"

# Print table header
printf "%-40s %-50s %-15s %-10s %-20s\n" "Namespace" "Pod" "Status" "Restarts" "Version"
printf "%-40s %-50s %-15s %-10s %-20s\n" "---------" "---------------------------------------" "---------" "--------" "-------"

# Get all namespaces with the prefix
namespaces=$(kubectl get namespaces --no-headers -o custom-columns=":metadata.name" | grep "^$NAMESPACE_PREFIX")

# Initialize previous_namespace variable to track changes between namespaces
previous_namespace=""

# Loop through each namespace
for namespace in $namespaces; do

  # Get all pods in the namespace
  pods=$(kubectl get pods -n $namespace --no-headers -o custom-columns=":metadata.name")

  # Loop through each pod
  for pod in $pods; do

    # Get pod status and restart count
    pod_status=$(kubectl get pod $pod -n $namespace -o jsonpath='{.status.phase}')
    restart_count=$(kubectl get pod $pod -n $namespace -o jsonpath='{.status.containerStatuses[?(@.name=="main")].restartCount}')

    # Get only the 'main' container in the pod and its image version
    kubectl get pod $pod -n $namespace -o jsonpath='{range .spec.containers[?(@.name=="main")]}{.image}{"\n"}{end}' | while read -r image; do
      version=${image##*:}

      # Only print the namespace if it's different from the previous one, with cyan color
      if [ "$namespace" != "$previous_namespace" ]; then
        printf "${CYAN}%-40s${NC}\n" "$namespace"
        previous_namespace=$namespace
      fi

      # Determine color for pod status
      case "$pod_status" in
        Running)
          status_color=$GREEN
          ;;
        Pending)
          status_color=$YELLOW
          ;;
        *)
          status_color=$RED
          ;;
      esac

      # Print the pod, status (with color), restarts, and version in a formatted table row
      printf "%-40s %-50s ${status_color}%-15s${NC} %-10s %-20s\n" "" "$pod" "$pod_status" "$restart_count" "$version"
    done
  done

  # Print a divider between namespaces
  echo "---------------------------------------------------------------------------------------------------------------------------------------------------"
done